package com.amdocs.webapp;

import com.agiletestingalliance.Duration;
import org.junit.Assert;
import org.junit.Test;

public class DurationTest {
	@Test
	public void testDesc_returnsExpectedDescriptionString() {
		// Arrange & Act
		Duration duration = new Duration();
		String actualDescription = duration.dur();

		// Assert
		Assert.assertTrue(actualDescription.contains("CP-DOF"));
	}

	@Test
	public void test_calculateIntValue(){
		Duration duration = new Duration();
		Integer value = duration.calculateIntValue();
		Assert.assertEquals(Integer.valueOf(5), value);

	}

}
