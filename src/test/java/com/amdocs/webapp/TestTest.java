package com.amdocs.webapp;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestTest {

	@Test
	public void testGstr() {
		String inputString = "Hello";
		com.agiletestingalliance.Test testInstance = new com.agiletestingalliance.Test(inputString);
		String result = testInstance.gstr();
		assertEquals(inputString, result);
	}
}