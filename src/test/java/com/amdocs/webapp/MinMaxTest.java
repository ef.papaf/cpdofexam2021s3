package com.amdocs.webapp;

import com.agiletestingalliance.Duration;
import com.agiletestingalliance.MinMax;
import org.junit.Assert;
import org.junit.Test;
public class MinMaxTest {
	@Test
	public void test_minmax(){
		MinMax minmax = new MinMax();
		Integer value = minmax.f(5,4);
		Assert.assertEquals(Integer.valueOf(5), value);

	}
}
