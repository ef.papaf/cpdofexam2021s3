package com.amdocs.webapp;

import com.agiletestingalliance.AboutCPDOF;
import org.junit.Assert;
import org.junit.Test;

public class AboutCPDOFTest {

	@Test
	public void testDesc_returnsExpectedDescriptionString() {
		// Arrange & Act
		AboutCPDOF cpDof = new AboutCPDOF();
		String actualDescription = cpDof.desc();

		// Assert
		Assert.assertTrue(actualDescription.contains("CP-DOF"));
	}
}

