package com.amdocs.webapp;

import com.agiletestingalliance.Usefulness;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class UsefulnessTest {

	@Test
	public void testDesc() {
		Usefulness usefulness = new Usefulness();
		String expectedDescription = "DevOps is about transformation";
		Assert.assertTrue(usefulness.desc().contains(expectedDescription));
	}

	@Test
	public void testWFMakingSureNoExceptionOccurs() {
		Usefulness usefulness = new Usefulness();
		usefulness.functionWF();
	}
}